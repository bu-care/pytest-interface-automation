#作者：道长
import allure
import sys

sys.path.append("..")

from api import  admin_api


class Admin():
    def __init__(self,admin_info):
        self.username = admin_info['user_name']
        self.password = admin_info['password']
        self.admin_api_client = admin_api.Admin()
        result = self.admin_login()
        print("result",result)
        assert result['status_code'] == 200,"总后台管理员登录失败"
        self.token = result['data']['data']['token']
        # self.session = result['data']['session']
    """-------------------------------iShow-admin--------------------------------------------------"""

    @allure.step("总后台账号登录")
    def admin_login(self):
        m = admin_api.Admin()
        return m.admin_login0(self.username,self.password)

    @allure.step("总后台账号登录-单接口测试")
    def admin_login1(self,**kwargs):
        m = admin_api.Admin()
        return m.admin_login0(**kwargs)

    @allure.step("总后台新增课程")
    def add_kecheng(self,**kwargs):
        return self.admin_api_client.admin_add_kecheng(self.token,**kwargs)

    @allure.step("总后台-上架课程")
    def shelve_course(self,**kwargs):
        return self.admin_api_client.admin_shelve_course(self.token,**kwargs)

    @allure.step("总后台-下架课程")
    def unshelve_course(self,**kwargs):
        return self.admin_api_client.admin_unshelve_course(self.token,**kwargs)


    """-------------------------------iShow-admin--------------------------------------------------"""
